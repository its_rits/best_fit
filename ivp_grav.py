""" 
Code that solves the initial value problem for a set of coupled ODEs.
Choose a range of f, for fixed values of C_D and k.

created by Ritali Ghosh
last modified 2020.11.25
"""

import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import solve_ivp
plt.rcParams.update({"axes.grid": True, 'grid.color': 'grey',
                     'grid.linestyle': '-.', 'grid.linewidth': 0.5})
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['savefig.dpi'] = 300
data = np.loadtxt("./data_files/analysis_pl3d.dat", comments='#')


def grav(t, y, model, chi, C_D, f, k):
    """
    \param[in] t: time step
    \param[in] y: [delta, v] 
    \param[in] model: 0 for analytical solution of a dense blob, else ours
    \return : f(y) = [frac{d\Delta}{dt}, frac{dv}{dt}]
    """

    delta, v = y
    if model == 0:  # PS05 soln
        fdelta = 0
        fv = (1. - 1./chi) - 3.*C_D/8./chi * delta**(-1./3.) * v**2
    else:           # our model
        fdelta = - v / f / chi**.5 * delta**(2./3.)
        fv = (1. - 1./chi) - 3.*C_D/8./chi * (delta + 1.e-15)**(-1./3.) * v**2 \
            - k * fdelta * v / delta
    return [fdelta, fv]


# parameters
C_D = np.array([1.], dtype=float)
f = np.array([2., 3., 4], dtype=float) # np.arange(2, 8, step=1, dtype=np.float)
k = np.array([0.0], dtype=float)

chi = 100
# scaling units
a = 10. * 3.085e18  # radius in cm ## 10 pc
g = 0.9 * 1.e14 / 3.085e21  # cm/s^2
v0 = np.sqrt(a * g) / 1.e5  # in km/s
t0 = np.sqrt(a / g) / (np.pi * 1.e13)  # Myr

"""---------------------- PLOT SIMULATED DATA ---------------------------"""
fig1, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 6), sharex=True)
plt.suptitle(r'IVP with gravity; $\chi $ = %3d' % chi)
model = 0   # PS05 soln
# initial conditions
ic = np.array([1., 0.])
# range within which solution is to be found
t_span = np.array([0, 80.])
# time axis
tau = np.linspace(0, 80., 1000)
sol100ps = solve_ivp(grav, t_span, ic,
                     dense_output=True, t_eval=tau, args=(model, chi, 0.75, 0., 0.))
delta100ps, v100ps = sol100ps.y
t100ps = sol100ps.t
ax1.plot(t100ps * t0, v100ps * v0, label=r"PS05 model($C_D$ =0.75)")
ax2.plot(t100ps * t0, delta100ps, label=r"PS05 model($C_D$ =0.75)")

""" simulation data """
UNIT_LENGTH = 3.085e21   # cm or 1kpc
UNIT_DENSITY = 1.67e-28  # g/cc
UNIT_VELOCITY = 100.     # km/s
UNIT_TIME = 9.81         # in Myr

ax1.plot(data[:, 0] * UNIT_TIME, data[:, 9] *
         UNIT_VELOCITY, "k-", label="Simulation data")
ax2.plot(data[:, 0] * UNIT_TIME, data[:, 10] /
         data[0, 10], "k-", label="Simulation data")
ax1.set_ylabel("Velocity [km/s]")
ax1.set_xlim(xmin=0., xmax=60.)
ax1.set_ylim(ymin=0.)
ax1.legend()
ax2.set_ylabel(r"$\Delta$ = $V_{cl}/V_{cl,0}$")
ax2.set_xlim(xmin=0., xmax=60.)
ax2.set_ylim(ymin=0.)
ax2.set_xlabel("Time in Myr")
ax2.legend()
#plt.savefig("grav_cd-75.png")
plt.show()
plt.close()

"""-------------------------- for different f -------------------------"""

fig2, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 6))
plt.suptitle(r'IVP with gravity; $\chi $ = %3d' % chi)
ax1.plot(data[:, 0] * UNIT_TIME, data[:, 9] *
         UNIT_VELOCITY, "k-", label="Simulation data")
ax2.plot(data[:, 0] * UNIT_TIME, data[:, 10] /
         data[0, 10], "k-", label="Simulation data")

for i in range(len(C_D)):
    for j in range(len(f)):
        for m in range(len(k)):
            # initial conditions
            ic = np.array([1., 0.])
            model = 1  # our model
            # range within which solution is to be found
            t_span = np.array([0., 80.])
            # time axis
            tau = np.linspace(0., 80., 1000)
            sol100 = solve_ivp(grav, t_span, ic,
                            dense_output=True, t_eval=tau, args=(model, chi, C_D[i], f[j], k[m]))
            delta100, v100 = sol100.y
            t100 = sol100.t
            ax1.plot(t100 * t0, v100 * v0, label=f"({C_D[i]},{f[j]},{k[m]})", lw=.75)
            ax2.plot(t100 * t0, delta100, label=f"({C_D[i]},{f[j]},{k[m]})", lw=.75)

ax1.set_ylabel("Velocity [km/s]")
ax1.set_xlim(xmin=0., xmax=60.)
ax1.set_ylim(ymin=0., ymax=150.)
ax1.legend(title=r"($C_D, f, \kappa$)")
ax2.set_ylabel(r"$\Delta$ = $V_{cl}/V_{cl,0}$")
ax2.set_xlabel("Time in Myr")
ax2.set_xlim(xmin=0., xmax=60.)
ax2.set_ylim(ymin=0., ymax=1.1)
ax2.legend(title=r"($C_D, f, \kappa$)")
#plt.savefig("grav_fit.png")
plt.show()
