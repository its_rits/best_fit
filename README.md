# Solving initial value problem for ODEs #

Repository with scripts for solving a set of coupled ordinary differential equations, with given initial conditions, for my project.  

*Dependencies*: standard python modules- numpy, matplotlib, scipy ([solve_ivp](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html)).