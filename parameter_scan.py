""" 
Code that solves the initial value problem for a set of coupled ODEs.
\params[in] C_D, f and k

created by Ritali Ghosh
last modified 2020.11.24
"""

import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import solve_ivp
plt.rcParams.update({"axes.grid": True, 'grid.color': 'grey',
                     'grid.linestyle': '-.', 'grid.linewidth': 0.5})
plt.rcParams['legend.fontsize'] = 8
plt.rcParams['savefig.dpi'] = 300


def cloud_cr(t, y, chi, C_D, f, k):
    delta, v = y
    #fdelta = - v * delta**(2./3.) / f / chi**.5
    fdelta = - v / f / chi**.5
    fv = - 3.*C_D/8./chi * (v)**2 * (delta + 1.e-15)**(-1./3.) - k * fdelta * v / delta
    return [fdelta, fv]


def growth(t, y, chi, C_D, f, k):
    delta, v = y
    t_cr = chi **.5
    if t < 2* t_cr:
        fdelta = - v / f / chi**.5
    else:
        fdelta = + v*(3./4./np.pi)* delta**(2./3.) /chi**.5
    fv = - 3.*C_D/8./chi * (v)**2 * (delta + 1.e-15)**(-1./3.) - k * fdelta * v / delta
    return [fdelta, fv]


# parameters
C_D = np.array([1.], dtype=float)
f = np.array([3,4,5], dtype=float)
k = np.array([0.], dtype=float)

chi = 100.
# in dimensionless units
t_cc = chi**0.5
t_drag = chi
# initial conditions
ic = np.array([1., 1.])

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
plt.suptitle('Cloud crushing, $\chi$= %03d' % (chi))
for i in range(len(C_D)):
    for j in range(len(f)):
        for m in range(len(k)):
            # range within which solution is to be found
            t_span = np.array([0., 4.*t_drag])
            # time axis
            tau = np.linspace(0., 4.*t_drag, 1000)

            '''no cooling'''
            sol100 = solve_ivp(cloud_cr, t_span, ic, dense_output=True,
                            t_eval=tau, args=(chi, C_D[i], f[j], k[m]))
            delta100, v100 = sol100.y
            time = sol100.t
            ax1.semilogy(time/t_cc, delta100, label=f"({C_D[i]},{f[j]},{k[m]}) no cooling")
            #ax2.plot(time/t_cc, v100, label=f"({C_D[i]},{f[j]},{k[m]}) no cooling")  # scaled with t_cc
            ax2.plot(time/t_drag, v100, label=f"({C_D[i]},{f[j]},{k[m]}) no cooling") # for time axis scaled with t_drag

            '''with cooling and mass growth'''
            sol100c = solve_ivp(growth, t_span, ic, dense_output=True,
                            t_eval=tau, args=(chi, C_D[i], f[j], k[m]))
            delta100c, v100c = sol100c.y
            timec = sol100c.t
            ax1.semilogy(timec/t_cc, delta100c, label=f"({C_D[i]},{f[j]},{k[m]})")
            #ax2.plot(time/t_cc, v100, label=f"({C_D[i]},{f[j]},{k[m]})")  # scaled with t_cc
            ax2.plot(timec/t_drag, v100c, label=f"({C_D[i]},{f[j]},{k[m]})") # for time axis scaled with t_drag


ax1.set_ylabel(r"$\Delta$ = $V_{cl}/V_{cl,0}$")
ax1.set_xlabel(r"$t/t_{cc}$")
ax1.set_xlim(xmin=0., xmax=30.)
ax1.set_ylim(.5*10**-1, 10**2)
ax1.legend(title=r"($C_D, f, \kappa$)")

ax2.set_ylabel(r"$\Delta v / v_{wind}$")
#ax2.set_xlabel(r"$t/t_{cc}$")  # scaled with t_cc
ax2.set_xlabel(r"$t/t_{drag}$")         # for time axis scaled with t_drag
#ax2.set_xlim(xmin=0., xmax=30.)  # scaled with t_cc
ax2.set_xlim(xmin=0., xmax=2.)          # for time axis scaled with t_drag
ax2.set_ylim(ymin=0.)
ax2.legend(title=r"($C_D, f, \kappa$)")
#plt.savefig("plot_parameters.png")
plt.show()
