""" 
Code that solves the initial value problem for a set of coupled ODEs.
Choose a range of k, for fixed values of C_D and f.

created by Ritali Ghosh
last modified 2020.11.20
"""

import numpy as np
import matplotlib.pylab as plt
from scipy.integrate import solve_ivp
plt.rcParams.update({"axes.grid": True, 'grid.color': 'grey',
                     'grid.linestyle': '-.', 'grid.linewidth': 0.5})
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['savefig.dpi'] = 300


def cloud_cr(t, y, chi, C_D, f, k):
    delta, v = y
    fdelta = - v * delta**(2./3.) / f / chi**.5
    fv = - 3.*C_D/8/chi * (v)**2 * (delta + 1.e-15)**(-1./3.) - k * v * fdelta / (delta)
    return [fdelta, fv]


# parameters
C_D = 2.
f = 2.
k = np.arange(0., 0.12, step=0.02, dtype=np.float)

chi = 100.
# in dimensionless units
t_cc = chi**0.5
t_drag = chi
# initial conditions
ic = np.array([1., 1.])

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 6))
plt.suptitle(
    "Cloud crushing without cooling, $ C_D $= %.2f, $ f $= %.2f " % (C_D, f))

# loop over different k's
for i in range(len(k)):
    # range within which solution is to be found
    t_span = np.array([0., 4.0*t_drag])
    # time axis
    tau = np.linspace(0., 4.*t_drag, 1000)

    sol100 = solve_ivp(cloud_cr, t_span, ic, dense_output=True,
                       t_eval=tau, args=(chi, C_D, f, k[i]))

    delta100, v100 = sol100.y
    time = sol100.t
    ax1.semilogy(time/t_cc, delta100, label=r"$ \kappa $=%.2f" % k[i])
    ax2.plot(time/t_cc, v100, label=r"$ \kappa $=%.2f" % k[i])
    # ax2.plot(time/t_drag, v100, label=r"$ \kappa $=%.2f" % k[i]) # for time axis scaled with t_drag

ax1.set_ylabel(r"$\Delta$ = $V_{cl}/V_{cl,0}$")
ax1.set_xlabel(r"$t/t_{cc}$")
ax1.set_xlim(xmin=0., xmax=30.)
ax1.set_ylim(.5*10**-1, 10**1)
ax1.legend()

ax2.set_ylabel(r"$\Delta v / v_{wind}$")
ax2.set_xlabel(r"$t/t_{cc}$")
# ax2.set_xlabel(r"$t/t_{drag}$")  # for time axis scaled with t_drag
ax2.set_xlim(xmin=0., xmax=30.)
# ax2.set_xlim(xmin=0., xmax=1.5)  # for time axis scaled with t_drag
ax2.set_ylim(ymin=0.)
ax2.legend()
plt.savefig("plot_cd-%.2f_f-%.2f.png" % (C_D, f))
plt.show()
